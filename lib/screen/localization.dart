import 'package:dio/locale/app_localization.dart';
import 'package:flutter/material.dart';
class Localization extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('سلام'),
        ),
        body: Center(
          child: Container(
            width: double.infinity,
              child: Text(
                  AppLocalization.of(context).heyWorld.toString(),
              ),
          ),
        ),
      ),
    );
  }
}
