import 'package:dio/screen/localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'locale/app_localization.dart';


void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  AppLocalizationDelegate _localeOverrideDelegate = AppLocalizationDelegate(Locale('fa',''));

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      locale: Locale("fa", ''),
      localizationsDelegates: [
        // ... app-specific localization delegate[s] here
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        _localeOverrideDelegate,
      ],
      supportedLocales: [
        const Locale('fa',''), // Persian
        const Locale('en',''), // English
      ],
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Localization(),
    );
  }
}
